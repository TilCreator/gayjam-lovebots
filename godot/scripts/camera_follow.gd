extends Camera2D

export var drag = 0.1
export var followSpeed = 1.0

var player: Node2D
var velocity: Vector2

func _process(delta):
    var distance = player.position - position

    velocity += distance * delta
    velocity -= velocity * drag

    position += velocity

func _on_bot_controller_signup(node):
    player = node
