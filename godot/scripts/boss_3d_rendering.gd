tool
extends Sprite


func _ready() -> void:
    pass


func _process(_delta: float) -> void:
    self.texture = (self.get_child(0) as Viewport).get_texture()
