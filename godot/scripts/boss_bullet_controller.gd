extends Node


var angle: float = 0
var speed: float = 400


func _ready() -> void:
    pass


func _process(delta: float) -> void:
    $bullet.global_transform.origin.x += self.speed * delta * cos(deg2rad(self.angle))
    $bullet.global_transform.origin.y += self.speed * delta * sin(deg2rad(self.angle))

    # TODO plz kill me
