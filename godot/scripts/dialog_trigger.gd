extends Area2D


export var record_name: String = ""
export var play_once_only: bool = false


func _ready():
    pass


func _on_trigger_body_entered(body: Node) -> void:
    if body.is_in_group("player"):
        $"../".find_node("Dialog_Player").play_dialog(self.record_name)

        if self.play_once_only:
            self.queue_free()
