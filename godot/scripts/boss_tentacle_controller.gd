extends Node


signal tentacle_stopped
signal tentacle_done
signal tentacle_death
signal tentacle_defeated


var angle_from: float = 0
var angle_to: float = 90
var poke_speed: float = 400
var rotation_speed_max: float = 150
var rotation_accelleration: float = 70
var length_target: float = 500
var stay_after_timer_max: float = 0.7

var rotation_speed_current: float = 0
var stay_after_timer: float = 0


func _ready() -> void:
    $tentacle/CollisionShape2D.shape = $tentacle/CollisionShape2D.shape.duplicate()
    $tentacle.rotation_degrees = angle_from


func _process(delta: float) -> void:
    if $tentacle/CollisionShape2D.shape.height != length_target:
        if $tentacle/CollisionShape2D.shape.height < length_target:
            $tentacle/CollisionShape2D.shape.height = min($tentacle/CollisionShape2D.shape.height + poke_speed * delta, length_target)
        else:
            $tentacle/CollisionShape2D.shape.height = max($tentacle/CollisionShape2D.shape.height - poke_speed * delta, length_target)

        $tentacle/CollisionShape2D.transform.origin.x = $tentacle/CollisionShape2D.shape.height / 2
    elif !($tentacle.rotation_degrees > angle_to - 0.5 and $tentacle.rotation_degrees < angle_to + 0.5):
        rotation_speed_current = min(rotation_speed_current + rotation_accelleration * delta, rotation_speed_max)

        if angle_to > angle_from:
            $tentacle.rotation_degrees = min($tentacle.rotation_degrees + rotation_speed_current * delta, angle_to)
        else:
            $tentacle.rotation_degrees = max($tentacle.rotation_degrees - rotation_speed_current * delta, angle_to)
    else:
        emit_signal("tentacle_stopped")
        if stay_after_timer >= stay_after_timer_max:
            emit_signal("tentacle_done")

            length_target = 0

            if $tentacle/CollisionShape2D.shape.height <= length_target:
                emit_signal("tentacle_death")
                self.queue_free()
        else:
            stay_after_timer += delta
