class_name BotController
extends RigidBody2D

#exporting variables for editor conigruration
export var walk_speed: float = 150.0
export var jump_speed: float = 150.0
export var max_roll_speed: float = 400.0
export var max_roll_acceleration: float = 400.0
export var max_boost_acceleration: float = 50.0
export var boost_acceleration: float = 600.0
export var hook_tolerance: float = 20
export var reload_time_secs: float = 1.0
export var switch_time: float = 0.5

#declaring signal to sign up self to camera
signal signup(node)

var players: Array
var ability_change_to: Dictionary

#loading abilities
var abilities = {
    BotState.MOVE: preload("res://scripts/abilities/movement.gd").new(),
    BotState.GRAPPLE: preload("res://scripts/abilities/grapple.gd").new(),
    BotState.BOOST: preload("res://scripts/abilities/boost.gd").new(),
    BotState.LASER: preload("res://scripts/abilities/laser.gd").new()
}

func _init():
    var player_input = preload("res://scripts/player_input.gd")
    players = [player_input.new(self, 1), player_input.new(self, 2)]
    abilities[BotState.MOVE].assign_player(players[0])
    abilities[BotState.BOOST].assign_player(players[1])

func _ready():
    gravity_scale = 0
    can_sleep = false
    emit_signal("signup", self)

func _integrate_forces(physics_state):
    if $"/root/BotState".inhibit:
        return

    # each module can integrate some forces onto the bot, in a way, that complys with godots best practice physics coding
    for state in abilities:
        abilities[state].integrate_forces(self, physics_state)
        
    if physics_state.get_contact_count() > 0:
        pass

func _process(delta):
    if $"/root/BotState".inhibit:
        return

    # each player can update
    for player in players:
        player.handle_input()

        if player.id in self.ability_change_to.keys():
            if self.ability_change_to[player.id]["time"] <= 0:
                self.ability_change_to[player.id]["time"] = max(self.ability_change_to[player.id]["time"] - delta, 0)
            elif self.ability_change_to[player.id]["target_ability"] != player.state:
                abilities[self.ability_change_to[player.id]["target_ability"]].assign_player(player)
                player.assign_state(self.ability_change_to[player.id]["target_ability"])

                self.ability_change_to.erase(player.id)
    
    # each module can update
    for state in abilities:
        abilities[state].process(self, delta)


func change_player_state(player, target_ability):
    if abilities[target_ability].unlocked:
        for ability in abilities:
            abilities[ability].remove_player(player)

        self.ability_change_to[player.id] = {
            "target_ability": target_ability,
            "time": switch_time
        }

func start_jump():
    abilities[BotState.MOVE].jump()

func end_jump():
    abilities[BotState.MOVE].land()
