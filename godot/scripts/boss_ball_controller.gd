extends Node


var radius_target: float = 0
var radius_current: float = 0
var angle: float = 0
var roll_out_speed: float = 30


func _ready() -> void:
    pass


func _process(delta: float) -> void:
    if self.radius_target != self.radius_current:
        self.radius_current += max(min(self.radius_target - self.radius_current, self.roll_out_speed * delta), -self.roll_out_speed * delta)

    var x = self.radius_current * cos(deg2rad(self.angle))
    var y = self.radius_current * sin(deg2rad(self.angle))

    $ball.transform.origin.x = x
    $ball.transform.origin.y = y
    $rope.points[1].x = x
    $rope.points[1].y = y
