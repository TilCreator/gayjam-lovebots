class_name MovementState

func enter(_movement) -> void:
    pass

func exit(_movement) -> void:
    pass

func integrate_forces(player_controller: RigidBody2D, input_direction: Vector2, state: PhysicsDirectBodyState) -> void:
    pass
