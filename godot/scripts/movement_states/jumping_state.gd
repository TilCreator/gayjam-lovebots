class_name JumpingState
extends MovementState

func integrate_forces(player_controller, input_direction, state):
    player_controller.apply_central_impulse(input_direction * 0.2 * player_controller.max_roll_acceleration * state.get_step())
