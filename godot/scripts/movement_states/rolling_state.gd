class_name RollingState
extends MovementState

func integrate_forces(player_controller, input_direction, state) -> void:
    if input_direction.length()>1:
        input_direction = input_direction.normalized()
    # sets the ration of the players current speed vs it's max speed
    var force_strength = player_controller.linear_velocity.length() / player_controller.max_roll_speed
    # calculates difference in direction of input and player movement
    var direction_difference = (input_direction.normalized() - state.linear_velocity.normalized()).length()
    
    force_strength = (1 - force_strength) + (force_strength * direction_difference)
    
    player_controller.apply_central_impulse(input_direction * force_strength * player_controller.max_roll_acceleration * state.get_step())
    
    
    #TODO: implement bounce
    if player_controller.get_colliding_bodies().size() > 0:
        state.linear_velocity = state.linear_velocity.bounce(state.get_contact_local_normal(0))
