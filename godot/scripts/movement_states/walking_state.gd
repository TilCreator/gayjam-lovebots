class_name WalkingState
extends MovementState

func integrate_forces(player_controller, input_direction, state) -> void:
    if player_controller.linear_velocity.length() > player_controller.walk_speed + 0.1:
        player_controller.linear_velocity *= 1-(state.step*3)
    else:
        player_controller.linear_velocity = input_direction * player_controller.walk_speed
