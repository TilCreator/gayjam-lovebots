class_name Movement
extends Ability
var boost_remaining = 1

var current_state: MovementState

func _init():
    current_state = MovementStates.walking

func integrate_forces(player_controller, physics_state) -> void:
    var input_direction: Vector2
    for input in assigned_players:
        input_direction += input.get_move_direction()
    
    if assigned_players.size() == 2 and input_direction.length()>1:
        input_direction *= 0.75
    
    current_state.integrate_forces(player_controller, input_direction, physics_state)

func process(player_controller, delta):
    if player_controller.abilities[BotState.MOVE].current_state != JumpingState and boost_remaining < 1:
        boost_remaining += delta
        
    if assigned_players.size() == 1:
        if assigned_players[0].get_use_strength() > 0:
            current_state = MovementStates.rolling
        else:
            current_state = MovementStates.walking

func jump(): current_state = MovementStates.jumping
    
func land(): current_state = MovementStates.walking
