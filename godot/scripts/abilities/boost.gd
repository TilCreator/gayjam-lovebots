class_name Boost
extends Ability
var boost_remaining = 2
var delay = 0
var boosting_players = []

func integrate_forces(bot_controller, state) -> void:
    if boosting_players.size() == 0: return
    
    for player in boosting_players:
        if player.get_aim_direction().length() > 0:
            if boost_remaining > 1.9:
                bot_controller.linear_velocity = player.get_aim_direction() * bot_controller.jump_speed
                boost_remaining -= state.step
                print(boost_remaining)
                print(state.step)
            elif boost_remaining > 0:
                var force_strength = player.get_use_strength() * pow((boost_remaining*0.5), 2.2) * state.step
                state.apply_central_impulse(player.get_aim_direction() * bot_controller.max_roll_acceleration * force_strength)
                boost_remaining -= force_strength
        if boost_remaining <= 0 or player.get_use_strength() == 0:
            boosting_players.erase(player)
            if delay == 0:
                delay = (1 - boost_remaining)/2
            if boosting_players.size() == 0:
                bot_controller.end_jump()
            

func process(bot_controller, delta) -> void:
    if delay <= 0:
        for player in assigned_players:
            if player.get_use_strength() > 0:
                bot_controller.start_jump()
                boosting_players.append(player)
    else:
        delay -= delta
        
    if boosting_players.size() == 0 and boost_remaining < 2:
        boost_remaining += delta

    
