class_name Ability

var assigned_players: Array
var unlocked: bool

func process(bot_controller: RigidBody2D, delta: float) -> void:
    pass

func integrate_forces(bot_controller: RigidBody2D, state: PhysicsDirectBodyState) -> void:
    pass

func assign_player(player) -> void:
    assigned_players.append(player)

func remove_player(player) -> void:
    assigned_players.erase(player)
