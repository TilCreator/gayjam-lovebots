class_name PlayerInput
var controller: RigidBody2D
var up: String
var down: String
var left: String
var right: String
var jump: String
var modes: Array
var use: String
var state: int = BotState.MOVE
var is_keyboard: bool = false
var id: int 

func _init(player_controller, player_id: int):
    self.id = player_id
    controller = player_controller
    
    # Initialises Names of pre-configured input actions depending on, what player is being generated.
    var player = "player%s" % id
    up = player + "_up"
    down = player + "_down"
    left = player + "_left"
    right = player + "_right"
    jump = player + "_jump"
    # TODO replace with better names
    modes = [
        player + "_mode0",
        player + "_mode1",
        player + "_mode2",
        player + "_mode3",
        ]
    use = player + "_use"

    # assignes second player to boost state
    if id == 2: state = BotState.BOOST

func get_move_direction() -> Vector2:
    # returns position of the joy stick or WASD-direction of keyboard
    return Vector2(
        Input.get_action_strength(right) - 
        Input.get_action_strength(left),
        Input.get_action_strength(down) - 
        Input.get_action_strength(up)
        )

func get_aim_direction() -> Vector2:
    # returns normalised joy stick position vector or vector of mouse relative to the bot
    if is_keyboard:
        return (controller.get_global_mouse_position() - controller.global_position).normalized()
    else:
        return get_move_direction().normalized()
        
func get_use_strength():
    return Input.get_action_strength(use)

func assign_state(state) -> void:
    self.state = state

func handle_input() -> void:
    for i in modes.size():
        if Input.is_action_just_pressed(modes[i]) and i != self.state:
            controller.change_player_state(self, i)
            break
