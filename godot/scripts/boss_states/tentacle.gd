extends Node


var fsm: StateMachine
var tentacle_scene: PackedScene = preload("res://scenes/subscenes/boss_tentacle_controller.tscn")

var indirect_poke_speed = 1000  # Poke speed of a sweep tentacle


func enter() -> void:
    var tentacle: Node2D
    var player_angle: float

    player_angle = self._get_player_angle()

    tentacle = tentacle_scene.instance()
    tentacle.poke_speed = indirect_poke_speed
    tentacle.angle_from = player_angle - 360/8
    tentacle.angle_to = player_angle + 360/8

    $"../../tentacles_container".add_child(tentacle)

    yield(tentacle, "tentacle_done")


    player_angle = self._get_player_angle()

    tentacle = tentacle_scene.instance()
    tentacle.angle_from = player_angle + 360/8
    tentacle.angle_to = player_angle - 360/8

    $"../../tentacles_container".add_child(tentacle)

    yield(tentacle, "tentacle_done")


    player_angle = self._get_player_angle()

    tentacle = tentacle_scene.instance()
    tentacle.poke_speed = indirect_poke_speed
    tentacle.stay_after_timer_max = 5
    tentacle.angle_from = player_angle
    tentacle.angle_to = player_angle

    $"../../tentacles_container".add_child(tentacle)

    yield(tentacle, "tentacle_stopped")


func _get_player_angle() -> float:
    return rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2


func process(_delta: float):
    pass


func physics_process(_delta: float):
    pass
