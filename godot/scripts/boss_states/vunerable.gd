extends Node


var fsm: StateMachine


func enter() -> void:
    fsm.change_to(@"defeated")


func process(_delta: float):
    pass


func physics_process(_delta: float):
    pass
