extends Node


var fsm: StateMachine


func enter() -> void:
    yield(get_tree().create_timer(2.0), "timeout")
    fsm.change_to(@"tentacle")


func process(_delta: float):
    pass


func physics_process(_delta: float):
    pass
