extends Node


var fsm: StateMachine
var bullet_scene: PackedScene = preload("res://scenes/subscenes/boss_bullet_controller.tscn")


func enter() -> void:
    var player_angle: float = 0
    # TODO Move to center

    # TODO Gun loading animation as a warning

    # Single spiral with limiter
    for _j in range(20):
        for i in range(4):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = i * (360 / 4)

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(0.1), "timeout")

    for _j in range(20):
        for i in range(8):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = i * (360 / 8)

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(0.1), "timeout")

    for j in range(360/2):
        var bullet: Node2D = bullet_scene.instance()
        bullet.angle = j * 7

        $"../../bullets_container".add_child(bullet)

        if j % 2 == 0:
            for i in range(8):
                bullet = bullet_scene.instance()
                bullet.angle = i * (360 / 8)

                $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(0.05), "timeout")

    # Crush player
    player_angle = rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2
    for j in range(50):
        for i in range(2):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = player_angle + (1 if i == 0 else -1) * max(1 - j/51.0, 0) * 360/8

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(0.1), "timeout")

    # half circle
    player_angle = rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2
    for _j in range(3):
        for i in range(45):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = player_angle - 360/4 + (360/2/45) * i

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(2.5), "timeout")

    # double circle with holes
    player_angle = rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2
    var safe_angle = rand_range(10, 360/2 - 10)
    for _j in range(2):
        for i in range(45):
            var angle = (360/2/45) * i
            if (angle > safe_angle + 10 or angle < safe_angle - 10) and randf() > 0.3:
                var bullet: Node2D = bullet_scene.instance()
                bullet.angle = player_angle - 360/4 + angle

                $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(1.0), "timeout")

    # multi spiral (changing direction)
    var number_of_spirals = 3
    for j in range(360/2):
        for i in range(number_of_spirals):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = j * 7 + 360/number_of_spirals * i

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(0.05), "timeout")

    # small bursts
    for _j in range(7):
        player_angle = rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2
        for i in range(10):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = player_angle - 360/15/2 + (360/15/10) * i

            $"../../bullets_container".add_child(bullet)

        yield(get_tree().create_timer(1.0), "timeout")

    # burst cloud
    for _j in range(7):
        player_angle = rad2deg($"../../.".transform.origin.angle_to_point($"../../../player_controller".transform.origin)) + 360/2
        for _i in range(10):
            var bullet: Node2D = bullet_scene.instance()
            bullet.angle = player_angle - 360/12/2 + (360/12/10) * rand_range(0, 10)

            $"../../bullets_container".add_child(bullet)

            yield(get_tree().create_timer(rand_range(0, 0.08)), "timeout")

        yield(get_tree().create_timer(1.0), "timeout")

    fsm.change_to(@"vunerable")


func process(_delta: float):
    pass


func physics_process(_delta: float):
    pass
