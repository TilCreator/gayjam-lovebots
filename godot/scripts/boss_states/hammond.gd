extends Node


var fsm: StateMachine
var ball_scene: PackedScene = preload("res://scenes/subscenes/boss_ball_controller.tscn")
var spinning_speed: float = 100
var spinning_speed_target: float = 0
var spinning_speed_current: float = 0
var spinning_accelleration: float = 20
var move_speed: float = 0.5
var move_speed_target: float = 0
var move_speed_current: float = 0
var move_accelleration: float = 0.25


func enter() -> void:
    spinning_speed_target = 0
    spinning_speed_current = 0
    move_speed_target = 0
    move_speed_current = 0

    # TODO Spinn animation as a warning

    var angle_offset = rand_range(0, 360)

    for i in range(3):
        var ball: Node2D = ball_scene.instance()
        ball.radius_target = rand_range(100, 200)
        ball.angle = i * 120 + angle_offset

        $"../../balls_container".add_child(ball)

        yield(get_tree().create_timer(0.5), "timeout")

    yield(get_tree().create_timer(1.0), "timeout")

    spinning_speed_target = spinning_speed

    yield(get_tree().create_timer(2.0), "timeout")

    move_speed_target = move_speed

    yield(get_tree().create_timer(10.0), "timeout")

    exit()


func exit():
    for ball in $"../../balls_container".get_children():
        ball.queue_free()

    fsm.change_to(@"defeated")


func process(delta: float):
    if len($"../../balls_container".get_children()) <= 0:
        self.exit()

    # TODO Check distance to wall

    if self.spinning_speed_target != self.spinning_speed_current:
        self.spinning_speed_current += max(min(self.spinning_speed_target - self.spinning_speed_current, self.spinning_accelleration * delta), -self.spinning_accelleration * delta)

    if self.move_speed_target != self.move_speed_current:
        self.move_speed_current += max(min(self.move_speed_target - self.move_speed_current, self.move_accelleration * delta), -self.move_accelleration * delta)

    for ball in $"../../balls_container".get_children():
        ball.angle += self.spinning_speed_current * delta

    $"../../.".transform.origin.x += move_speed_current


func physics_process(_delta: float):
    pass
