# Inspired and partialy copied from https://gdscript.com/solutions/godot-state-machine/
extends Node
class_name StateMachine


var state: Object
var history: Array = []


func _ready() -> void:
    # Set the initial state to the first child node
    state = get_child(0)
    _enter_state()


func change_to(new_state: NodePath) -> void:
    history.append(state.name)
    state = get_node(new_state)
    _enter_state()


func back() -> void:
    if history.size() > 0:
        state = get_node(history.pop_back())
        _enter_state()


func _enter_state() -> void:
    if OS.is_debug_build():
        print("Entering state: ", state.name)
    # Give the new state a reference to this state machine script
    state.fsm = self
    state.enter()


# Route Game Loop function calls to
# current state handler method if it exists
func _process(delta):
    if state.has_method("process"):
        state.process(delta)


func _physics_process(delta):
    if state.has_method("physics_process"):
        state.physics_process(delta)
