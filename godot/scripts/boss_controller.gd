extends Node


var health: int = 3 setget ,health_get


func _ready() -> void:
    randomize()


func health_get() -> int:
    return health
